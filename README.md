# Projeto PROJETOROBOTWEB

Aplica teste funcional de um CRUD no site  http://agriness-challenge.herokuapp.com/ , utilizando as ferramentas: selenium Web Drive e Robot Framework.


# Funcionalidades testadas
* Cadastro de usuario
* Novo particpante
* Buscar participante
* Editar participante
* Excluir participante

# Requisitos
* [Phyton 3.8+](https://www.python.org/downloads/)
* Chrome 83+
* [Chromedriver 85+](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

# Instalando dependências
``` pip install -r requirements.tx  ```

# Executando o projeto
``` robot -d ./report Tests\ ```

Serão executados todos os casos de testes e os detalhes com os prints do resultado de cada caso de teste estarão disponíveis em relatório HTML no diretório /report.
