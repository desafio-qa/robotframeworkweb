*** Settings ***
Resource             ../Resource/Resource.robot
Resource             ../Resource/Pages/Novo_participante.robot
Resource             ../Resource/Pages/Home.robot
Test Setup           Nova sessão   
Test Teardown        Encerra sessão

*** Test Cases ***

Novo participante
	[tags]   novo   
    Dado que acessei o formulário de cadastro de novo participante
    Quando eu preencher os campos obrigatorios
    E clicar em cadastrar
    Então vejo o cadastro realizado com sucesso

Buscar participante
    [tags]     buscar
    Dado que acessei a pagina home
    Quando eu buscar pelo participante
    Então a busca deve ser realizada com sucesso

Editar participante
    [tags]   editar 

    Dado que pesquisei por um participante
    Quando eu clicar em editar
    E editar a data de nasncimento
    Então a edição deve ser realizada com sucesso

Excluir participante
    [tags]   excluir 

    Dado que pesquisei por um participante
    Quando eu clicar em excluir
    E confirmar a exclusão
    Então uma mensagem de confirmação de exclusão deve ser apresentada