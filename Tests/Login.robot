*** Settings ***
Resource             ../Resource/Resource.robot
Resource             ../Resource/Pages/Login.robot
Test Setup           Nova sessão   
Test Teardown        Encerra sessão
Suite Setup          Initialize Random Variables      

*** Test Cases ***

    
Cenário: CTO1- Cadastrar usuário
    [tags]   cadastro 
    Dado que acessei a pagina de Login
    Quando eu preencho os campos obrigatorios
    E clico em Cadastrar
    Então meu nome é apresentado

