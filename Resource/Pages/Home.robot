 
*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${home_url}                          http://agriness-challenge.herokuapp.com/home
${home_busca}                        id:buscaNome
${home_botao_buscar}                 id:botaoBuscar
${home_table}                        xpath:.//tr[contains(.,'Raquel C D')]
${home_botao_editar}                 id:editarContato
${home_nome}                         id:full-name
${home_salvar_edicao}                id:editar
${home_botao_excluir}                id:deletarContato
${home_participante_removido}        xpath=//*[contains(text(), "Participante removido com sucesso com sucesso.")]
${home_participante_alterado}        xpath=//*[contains(.,'Participante alterado com sucesso.')]



*** Keywords ***

Dado que acessei a pagina home
    Go to                              ${home_url}
    Wait Until Location Is             ${home_url}

Quando eu buscar pelo participante

    Wait Until Element Is Visible      ${home_busca} 
    input Text                         ${home_busca}    Raquel C D
    Click Element                      ${home_botao_buscar} 

Então a busca deve ser realizada com sucesso
    Wait Until Element Is Visible      ${home_table} 
    ${target}=      Get WebElement     ${home_table}      
    Should contain                     ${target.text}    Raquel C D


Dado que pesquisei por um participante
    Go to                              ${home_url}
    Wait Until Location Is             ${home_url}
    Wait Until Element Is Visible      ${home_busca} 
    input Text                         ${home_busca}    Raquel C D
    Click Element                      ${home_botao_buscar} 

Quando eu clicar em editar

    Wait Until Element Is Visible      ${home_botao_editar}
    Click Element                      ${home_botao_editar}
    
E editar a data de nasncimento
    
    Wait Until Element Is Visible      ${novoParticipante_nascimento} 
    input Text                         ${novoParticipante_nascimento}       10102022
    sleep                              2s
    Click Element                      ${home_salvar_edicao}

Então a edição deve ser realizada com sucesso
    sleep            5s
    Wait Until Element Is Visible      ${home_participante_alterado}
    Should Contain                     ${home_participante_alterado}    Participante alterado com sucesso.   


Quando eu clicar em excluir
    Wait Until Element Is Visible      ${home_botao_excluir} 
    Click Element                      ${home_botao_excluir} 

E confirmar a exclusão
    Alert Should Be Present           action=ACCEPT      timeout=None
    

Então uma mensagem de confirmação de exclusão deve ser apresentada
    sleep            5s
    Wait Until Element Is Visible     ${home_participante_removido}
    Should Contain                    ${home_participante_removido}   Participante removido com sucesso com sucesso.        