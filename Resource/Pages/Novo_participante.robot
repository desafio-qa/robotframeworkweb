*** Settings ***
Library         SeleniumLibrary


*** Variables ***

${novoParticipante_url}                       http://agriness-challenge.herokuapp.com/novoParticipante   
${novoParticipante_nome_Completo}             id:full-name  
${novoParticipante_nascimento}                id:date-birth  
${novoParticipante_RG}                        id:rg  
${novoParticipante_CPF}                       id:cpf  
${novoParticipante_botao_Cadastrar}           id:cadastrar
${novoParticipante_cadastrado}                xpath=//*[contains(text(), "Participante cadastrado com sucesso.")]

*** Keywords ***

Dado que acessei o formulário de cadastro de novo participante
    
    Go to                                  ${novoParticipante_url}    
    Wait Until Location Is                 ${novoParticipante_url}     
  

Quando eu preencher os campos obrigatorios
    sleep             5s
    input Text        ${novoParticipante_nome_Completo}           Raquel C D
    input Text        ${novoParticipante_nascimento}              12111988
    input Text        ${novoParticipante_CPF}                     12345678978
    input Text        ${novoParticipante_RG}                      123456

E clicar em cadastrar

    Click Element               ${novoParticipante_botao_Cadastrar}
    

Então vejo o cadastro realizado com sucesso

    Should Contain              ${novoParticipante_cadastrado}   Participante cadastrado com sucesso.    

    