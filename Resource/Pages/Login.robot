*** Settings ***
Library         SeleniumLibrary
Library         String


*** Variables ***

${login_url}                            http://agriness-challenge.herokuapp.com/login     
${login_cadatro_nome_usuario}           id:cadastroNome
${login_cadastro_email}                 id:cadastroEmail
${login_cadastro_senha}                 id:cadastroSenha     
${login_botao_cadastrar}                id:botaoCadastrar
${login_header}                         xpath=.//span[contains(.,'Olá,')]          


*** Keywords ***

Initialize Random Variables
    ${login_random_name}=   Generate random string  8  [LOWER]
    Set global variable  ${login_random_name}

Dado que acessei a pagina de Login
    Go to   ${login_url} 

Quando eu preencho os campos obrigatorios
    Input Text                       ${login_cadatro_nome_usuario}         ${login_random_name}
    Input Text                       ${login_cadastro_email}               ${login_random_name}@agriness
    Input Text                       ${login_cadastro_senha}               Robot123456

E clico em Cadastrar

    Click Element       ${login_botao_cadastrar}

Então meu nome é apresentado

    Wait Until Element Is Visible               ${login_header} 
    ${target}=      Get WebElement              ${login_header}      
    Should contain                              ${target.text}          ${login_random_name}
